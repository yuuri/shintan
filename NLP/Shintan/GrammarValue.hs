{-# LANGUAGE TemplateHaskell #-}

module NLP.Shintan.GrammarValue where

import qualified Data.Map as M
import Text.Printf
import Data.Function (on)

import NLP.Shintan.Morpho

defGrammarValue "GValue"
   [ ( "Pos"     --Часть речи
     , [ "Noun"  --Существительное
       , "Pron"  --Местоимение
       , "Verb"  --Глагол
       , "Post"  --Послелог
       , "Adj"   --Прилагательное
       , "Adv"   --Наречие
       , "Conj"  --Союз
       , "Ptcl"  --Частица
       , "Subst" --Субстантиватор
       , "Root"  --Корень предложения
       ]
     )
   , ( "Number" --Число
     , [ "Sg"   --Единственное
       , "Pl"   --Множественное
       ]
     )
   , ("Casus"    --Падеж
     , [ "Nom"   --nominative (topic), は
       , "Rem"   --nominative (rematic), が
       , "Gen"   --genitive, の
       , "Dat"   --dative, に
       , "Acc"   --accusative, を
       , "Instr" --instrumentalis, で
       , "All"   --allative, へ
       , "Term"  --terminative, まげ
       , "Comp"  --comparative, より
       , "Abl"   --ablative, から
       , "Coop"  --cooperative, と
       , "Com"   --comitative, も
       ]
     )
   , ( "Person"  --Лицо
     , [ "First"  --Первое
       , "Second" --Второе
       , "Third" --Третье
       ]     
     )
   , ("Gender"  --Род
     , [ "Masc" --Мужской
       , "Fem"  --Женский
       ]
     )
   , ("Tense" --Время
     , [ "Past" --Прошедшее
       , "Pres" --Настояще-будущее
       ]   
     )
   , ("FormVerb" --Форма глагола
     , [ "Finit" --Завершённый
       , "Inf"   --Инфинитив
       , "AdvPartPast" --Деепричастие предшествования, -て
       , "Repres"      --Репрезентативность, -たり
       ]
     )
   , ("Mood" --Наклонение
     , [ "Indic" --Изъявительное
       , "Imper" --Побудительное
       , "Prob"  --Вероятностное
       , "Des"   --Желательное
       ]       
     )
   , ("Voice" --Залог
     , [ "Act"        --Действительный
       , "Pass"       --Пассивный
       , "Impell"     --
       , "PassImpell" --
       ]
     )
   , ("PronType" --Тип местоимения
     , [ "PronAdj"      --Прилагательное ("такой")
       , "PronPersonal" --Личное ("я")
       , "PronDemonstr" --Указательное ("этот")
       , "PronRelative" --Относительное ("так")
       , "PronNumeral"  --Числительное ("столько")
       ]      
     )
   , ("Affirm" --Утверждение
     , [ "Pos" --Утвердительное
       , "Neg" --Отрицательное
       ]   
     )
   , ("Proper" --Тип собственного имени
     , [ "General"
       , "Town"    --Город
       , "Lake"    --Озеро
       , "SurName" --Имя или фамилия
       , "River"   --Река
       ]
     )
   ]
                   
-- Word datatype --

data Word = Word
    { wordStr :: String
    , wordPosition :: Int
    , wordGV :: GValue
    }

instance Show Word where
    show (Word str pos gv) = printf "{%s:%d %s}" str pos (show gv)

wordPOS = pos . wordGV

instance Eq Word where
    (==) = (==) `on` wordPosition

instance Ord Word where
    (<=) = (<=) `on` wordPosition
    (<) = (<) `on` wordPosition
